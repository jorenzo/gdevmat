
Mover mover;

void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  mover = new Mover();
  mover.velocity = new PVector(.3, 0);
  mover.position = new PVector(Window.left+50, 0);
  mover.acceleration = new PVector(.2, 0);
  mover.r = 155;
  mover.g = 55;
  mover.b = 180;
  mover.a = 50;
}

void draw()
{
  background(255);
  mover.render();
  
  if(mover.position.x > Window.right)
  {
    mover.position.x = Window.left + 50;
  }
}
