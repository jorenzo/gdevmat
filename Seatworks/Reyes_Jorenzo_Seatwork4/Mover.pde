public class Mover
{
  public PVector position = new PVector();
  public PVector velocity = new PVector();
  public PVector acceleration = new PVector();
  public float scale = 50;
  public float r = 155, g = 55, b = 180, a = 50;
  
  Mover()
  {
  }
  
  public void render()
  {
    update();
    fill(r,g,b,a);
    circle(position.x, position.y, scale);
  }
  
  private void update()
  {
    if(mover.position.x < 0)
      this.velocity.add(this.acceleration);
    
    if(mover.position.x >= 0)
      this.velocity.sub(this.acceleration);
    
    if(mover.velocity.x <= 0)
      mover.velocity.x = 2;
    
    this.position.add(this.velocity);
  }
  
}
