// called for initialization of first frame
void setup()
{
  size(1920, 1080, P3D);    // P3D makes the environment in the window 3D
  
  // size(1080, 720, P3D); for laptops
  
  // Moves the camera:
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180),    // camera position
          0, 0, 0,      // eye position
          0, -1, 0);    // up vector
}

Walker walker = new Walker();

void draw()
{
  walker.render();
  walker.randomWalk();
  
  //float randomValue = floor(random(1,4));
  //println(randomValue);
}
