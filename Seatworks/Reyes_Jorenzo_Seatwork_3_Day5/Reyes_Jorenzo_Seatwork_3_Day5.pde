void setup()
{
  size(1920, 1080, P3D);    // P3D makes the environment in the window 3D
  background(255);
  // size(1080, 720, P3D); for laptops
  
  // Moves the camera:
  camera(0, 0, Window.eyeZ,    // camera position
          0, 0, 0,      // eye position
          0, -1, 0);    // up vector
}


Vector2 position = new Vector2();
Vector2 velocity = new Vector2(5, 8);
int decision = floor(random(8));
Walker w = new Walker();

Vector2 mousePos()
{
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  return new Vector2(x, y);
}

void draw()
{
  Vector2 mouse = mousePos();
  
  mouse.normalize();
  mouse.mult(500);
  background(0); // clear every frame
  
  strokeWeight(22);
  stroke(255,0,0,random(255));
  line(0, 0, mouse.x, mouse.y);
  line(0, 0, -mouse.x, -mouse.y);
  
  strokeWeight(10);
  stroke(255,255,255);
  line(0, 0, mouse.x, mouse.y);
  line(0, 0, -mouse.x, -mouse.y);
  
  strokeWeight(22);
  stroke(180,180,180);
  line(0, 0, mouse.x/5, mouse.y/5);
  line(0, 0, -mouse.x/5, -mouse.y/5);
  
  strokeWeight(10);
  stroke(120,120,120);
  line(0, 0, mouse.x/15, mouse.y/15);
  
  strokeWeight(5);
  stroke(150,150,150);
  line(0, 0, mouse.x/25, mouse.y/25);
  
  
  println("Magnitude: " + mouse.mag());
}
