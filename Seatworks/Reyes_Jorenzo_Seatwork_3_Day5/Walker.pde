class Walker
{
  Vector2 position = new Vector2(0, 0);
  Vector2 velocity = new Vector2(5,8);
  
  
  void render()
  {
    fill( random(255), random(255), random(255), random(50,100));
    circle(position.x, position.y, 30);
  }
  
  void randomWalk()
  {
    int decision = floor(random(8));
    
    position.add(velocity);
    
    // going up
    if(decision == 0)
    {
      velocity.y *= 1;
    }
    
    else if(decision == 1)
    {
      velocity.y *= -1;
    }
    
    else if(decision == 2)
    {
      velocity.x *= 1;
    }
    
    else if(decision == 3)
    {
      velocity.x *= -1;
    }
    
    else if(decision == 4)
    {
      velocity.x *= 1;
      velocity.y *= 1;
    }
    
    else if(decision == 5)
    {
      velocity.x *= -1;
      velocity.y *= -1;
    }
    
    else if(decision == 6)
    {
      velocity.x *= 1;
      velocity.y *= -1;
    }
    
    else if(decision == 7)
    {
      velocity.x *= -1;
      velocity.y *= 1;
    }
  }
}
