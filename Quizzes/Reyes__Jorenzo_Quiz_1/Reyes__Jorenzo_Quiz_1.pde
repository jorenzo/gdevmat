// called for initialization of first frame
void setup()
{
  size(1920, 1080, P3D);    // P3D makes the environment in the window 3D
  background(255);
  // size(1080, 720, P3D); for laptops
  
  // Moves the camera:
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180),    // camera position
          0, 0, 0,      // eye position
          0, -1, 0);    // up vector
}

Splatter splatter = new Splatter();

void draw()
{
  
  float mean = 0;
  float std = 250;
  float gauss = randomGaussian();
  
  float x = std * gauss + mean;
  
  float mean2 = 0;
  float std2 = random(100);
  float gauss2 = randomGaussian();
  
  float size = std2 * gauss2 + mean2;
  
  noStroke();
  splatter.render(x, size);
  splatter.randomWalk();
}
