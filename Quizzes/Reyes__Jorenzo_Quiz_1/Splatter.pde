class Splatter
{
  float xPos;
  float yPos;
  float frames = 0;

  void render(float x,float size)
  {
    fill( random(255), random(255), random(255), random(50,100));
    circle(x, yPos, size);
    Clear();
  }
  
  void randomWalk()
  {
    float decision = random(-500, 500);
    yPos = decision;
  }
  
  void Clear()
  {
    frames++;
    if(frames % 1000 == 0)
    background(255);
  }
}
