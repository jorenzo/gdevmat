class Walker
{
  int widthPx = 1980;
  int heightPx = 1080;
  
  float xPos = 50;
  float yPos = 50;
  
  float y = yPos;
  float x = xPos;
  
  float t = 0;
  
  float r_t = 100;
  float g_t = 150;
  float b_t = 200;
  
  float radMax = 200;
  float posMax = 300;
  float dist = 50;
  
  void render()
  { 
    noStroke();
    float nX = noise(x);
  
    float nY = noise(y);
  
    float nS = noise(t); 
    float xS = map(nS, 0, 1, 0, radMax);
    
    float nR = noise(r_t);
    float xR = map(nR, 0, 1, 0, 255);
    float nG = noise(g_t);
    float xG = map(nG, 0, 1, 0, 255);
    float nB = noise(b_t);
    float xB = map(nB, 0, 1, 0, 255);
    fill(xR, xG, xB);
    
    circle(xPos, yPos, xS);
    
   int decision = floor(random(8));
    
    if(decision == 0)
    {
      yPos += map(nY, 0, 1, 0, dist);
    }
    
    else if(decision == 1)
    {
      yPos -= map(nY, 0, 1, 0, dist);
    }
    
    else if(decision == 2)
    {
      xPos += map(nX, 0, 1, 0, dist);
    }
    
    else if(decision == 3)
    {
      xPos -= map(nX, 0, 1, 0, dist);
    }
    
    else if(decision == 4)
    {
      xPos += map(nX, 0, 1, 0, dist);
      yPos += map(nY, 0, 1, 0, dist);
    }
    
    else if(decision == 5)
    {
      xPos -= map(nX, 0, 1, 0, dist);
      yPos -= map(nY, 0, 1, 0, dist);
    }
    
    else if(decision == 6)
    {
      xPos += map(nX, 0, 1, 0, dist);
      yPos -= map(nY, 0, 1, 0, dist);
    }
    
    else if(decision == 7)
    {
      xPos -= map(nX, 0, 1, 0, dist);
      yPos += map(nY, 0, 1, 0, dist);
    }
    
    y += 0.09;
    x += 0.09;
    
    r_t += 0.02;
    g_t += 0.02;
    b_t += 0.02;
    
    t += 0.01; // rate of change
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  void randomWalk()
  {
    int decision = floor(random(8));
    
    if(decision == 0)
    {
      yPos+=5;
    }
    
    else if(decision == 1)
    {
      yPos-=5;
    }
    
    else if(decision == 2)
    {
      xPos+=5;
    }
    
    else if(decision == 3)
    {
      xPos-=5;
    }
    
    else if(decision == 4)
    {
      xPos+=5;
      yPos+=5;
    }
    
    else if(decision == 5)
    {
      xPos-=5;
      yPos-=5;
    }
    
    else if(decision == 6)
    {
      xPos+=5;
      yPos-=5;
    }
    
    else if(decision == 7)
    {
      xPos-=5;
      yPos+=5;
    }
  }
}
