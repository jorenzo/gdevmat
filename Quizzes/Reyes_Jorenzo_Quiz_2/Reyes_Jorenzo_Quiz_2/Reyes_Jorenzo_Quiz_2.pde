// called for initialization of first frame
void setup()
{
  size(1920, 1080, P3D);    // P3D makes the environment in the window 3D
  
  // size(1080, 720, P3D); for laptops
  
  // Moves the camera:
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180),    // camera position
          0, 0, 0,      // eye position
          0, -1, 0);    // up vector
          background(255);
}

void draw()
{
  //float n = noise(t); // generate noise over time
  //float x = map(n, 0, 1, 0, Window.top);
  //rect(Window.left + (t * 100), Window.bottom, 1, x);
  
  w.render();
}
