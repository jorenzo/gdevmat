public static class Window
{
  public static int widthPx = 1920;
  public static int heightPx = 1080;
  public static int windowWidth = widthPx/2;
  public static int windowHeight = heightPx/2;
  public static int top = windowHeight;
  public static int bottom = -windowHeight;
  public static int left = -windowHeight;
  public static int right = windowWidth;
  public static int NE = windowHeight + windowWidth;
  public static int NW = windowHeight - windowWidth;
  public static int SE = -windowHeight + windowWidth;
  public static int SW = -windowHeight - windowWidth;
}
